---
title: "DJ Set: Surprise Duo"
category: Event
date: 2022-07-15 23:00:00
outputs: [HTML, Calendar]
---
![undisclosed artists](undercover-duo.jpg)

Wow, a surprise! Who could this be?! Join in on the demoscene's own
version of The Masked Singer where we all guess which two fine gentlemen
(or gentlewomen, we don't discriminate!) are behind the masks!

Or maybe we'll just enjoy some high-BPM quality music that would make
Ile/Aardbei pull out his old [clown-dance]!

[clown-dance]: http://www.slengpung.com/?id=10738
