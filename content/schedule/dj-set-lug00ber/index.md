---
title: "DJ Set: lug00ber"
category: Event
date: 2022-07-16 23:00:00
outputs: [HTML, Calendar]
---
![lug00ber](lug00ber.jpg)

For the last couple of decades, lug00ber has been a regular contributor
to both Norwegian and international demoparties, both as a music producer
as well as a DJ.

He mostly plays drum & bass, but also sometimes goes off into jungle,
dancehall, reggae and dub. Or even eurodance if you catch him in a
particularly odd mood!

He streams DJ sets on Twitch every Tuesday - make sure to join in on the
fun!

Links:
- http://lug00ber.m0f0.net/
- https://www.mixcloud.com/lug00ber/
- https://www.twitch.tv/lug00ber
