---
title: Schedule
outputs: [HTML, Calendar]
menu:
  main:
    weight: 4
  footer:
    parent: pages
    weight: 4
cascade:
  outputs: [Calendar]
  publishDate: 2022-06-12
---

Here's this years time-table. Please note that this is only an estimate,
and events might be moved or delayed due to unforseen circumstances.
Arranging a demo-party isn't an exact science.

*All times are in local time, [CEST (UTC+2)](https://time.is/CEST).*
