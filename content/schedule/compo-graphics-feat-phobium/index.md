---
title: Graphics compo block feat. Phobium
category: Compos
date: 2022-07-16 15:00:00
outputs: [HTML, Calendar]
---
![Phobium](phobium.jpg)

During the graphics competitions, Phobium will be on the decks playing
some relaxing music for you to enjoy while watching the graphics entries.

Links:

- https://phobium.net/
- https://www.facebook.com/phobiummusic
- https://soundcloud.com/phobium
