---
Title: Contact
menu:
  main:
    weight: 5
  footer:
    parent: pages
    weight: 6
---

If you need to get in touch with us, you can use these:

- General questions: <contact@blackvalley.party>.
- Compo team: <compos@blackvalley.party>.

We can also be reached through our social media accounts, which you
can find linked at the bottom of the page.

### Norsk Demopartyforening

Black Valley is organized by [Norsk Demopartyforening][demoparty.no],
a non-profit organization that that has the goal of furthering
[demoparties] in Norway.

You can reach Norsk Demopartyforening via email at
<contact@demoparty.no>. Other relevant information can be found in
[Brønnøysundregisterene] (Norwegian language only, sorry).

[demoparty.no]: https://demoparty.no/
[demoparties]: https://en.wikipedia.org/wiki/Demoscene#Parties
[Brønnøysundregisterene]: https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=928153193
