---
Title: Home
menu:
  footer:
    parent: pages
    weight: 1
---

## About

Black Valley is a new [demoparty] in Oslo, Norway - focused on bringing
the summer demoparty vibe back where it belongs! Brought to you by
experienced organizers from previous editions of [Solskogen],
[Kindergarden] and [Evoke] plus some fresh faces, we have the utmost
pleasure of inviting you to join us this July.

The party has been sold out, but thanks to our awesome friends at
[SceneSat], a stream of our party be available for free!

Please note that due to legal regulations Black Valley does not allow
sleeping at the venue. You can find opening hours for the venue in our
[schedule]. Sleeping accomodations need to be found separately, but there
are [airbnbs] and [hotels] nearby.

The venue provides a full bar for your beverage needs. This means you need
to be 20 years of age to attend, and bringing your own alcohol is strictly
prohibited.

[demoparty]: https://en.wikipedia.org/wiki/Demoscene#Parties
[Solskogen]: https://solskogen.no/
[Kindergarden]: https://www.demoparty.net/kindergarden
[Evoke]: https://www.evoke.eu/
[schedule]: {{< ref "schedule.md" >}}
[airbnbs]: https://www.airbnb.no/s/Kroloftet-Oslo-Badstuforening--Arnljot-Gellines-vei--Oslo--Norge/homes?tab_id=home_tab&refinement_paths%5B%5D=%2Fhomes&flexible_trip_lengths%5B%5D=one_week&date_picker_type=calendar&checkin=2022-07-15&checkout=2022-07-17&source=structured_search_input_header&search_type=autocomplete_click&query=Kroloftet%20Oslo%20Badstuforening%2C%20Arnljot%20Gellines%20vei%2C%20Oslo%2C%20Norge&place_id=ChIJgwLbRnlvQUYRulb5zk13i-o
[hotels]: https://www.scandichotels.com/hotels/norway/oslo/scandic-helsfyr
[SceneSat]: https://scenesat.com/
