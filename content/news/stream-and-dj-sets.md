---
Title: Stream and DJ Sets
Date: 2022-07-05
icon: img/musical-notes.svg
---
[SceneSat] are coming to Black Valley, and they will help us setting
up a stream of the party!

We've also announced two DJs who will be performing DJ-sets during the
party! Additionally, there will also be an surprise DJ duo playing on
Friday evening. See our [schedule] for more information!

[SceneSat]: https://scenesat.com/
[schedule]: {{< ref "/schedule.md" >}}

<!--more-->
