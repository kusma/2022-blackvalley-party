---
Title: Sold out
Date: 2022-06-18
icon: img/ticket.svg
---

We are officially sold out!

<!--more-->

To everyone who was on the ball and got a ticket, a huge thank you! In
just a few short weeks, we are so looking forward to making an awesome
party for each and every one of you!

For anyone wondering, unfortunately no more tickets will be made
available. The size of the venue is limited, so we are at max capacity.

If you did not get a ticket in time, but still want to help us out
financially for this edition and future editions - you can still get a
[donation ticket]. This really helps us get on our feet, as a young
party building up.

[donation ticket]: https://fienta.com/black-valley-2022
