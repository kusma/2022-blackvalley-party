---
Title: More tickets!
Date: 2022-06-21
icon: img/ticket.svg
---

Due to the overwhelming demand for tickets, we've tried to find some
solutions so that we can welcome a few more people at Black Valley this
year. Since we do not have unlimited space at the venue, we have already
made compromises on the amount of tables vs just chairs, and there will be
no reserved seating available. Since we close the venue during the night,
we expect everyone to take their stuff with them when they leave for the
night.

<!--more-->

However, we have made a small change in the way we set up the space, and
we will keep the table allotment a bit more dynamic - meaning we'll have
more tables out for those that want to work on their stuff during the
daytime, then pack some of those tables away during the late evenings for
the compo showings and DJ sets.

This way, we can squeeze a some more people in. We are therefore
announcing that this coming Friday at 18:00, we will release a few more
tickets. The tickets went *fast* last time - so you better act fast!
