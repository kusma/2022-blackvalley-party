---
Title: Copy That Floppy!
Date: 2022-07-07
icon: img/save.svg
---

As some of you know, Menace and Dozer have been dumping disks to preserve
demoscene materials at most recent Solskogen editions - and this year this
service returns at Black Valley.

<!--more-->

We are part of an international group of dumpers from all over, across all
platforms. We have extensive knowledge and hardware to dump floppy disks,
tapes, CD/DVD/Blu-ray, backup hardware or old hard drives. For any
questions, please come and talk to either of us on the [Demozoo discord]
and also visit our channel #diskdumping over there.

Due to our organizing tasks this year, there will likely be no live
dumping directly, but we will be there and we will be accepting both small
and large collections.

[Demozoo discord]: https://discord.io/demozoo
