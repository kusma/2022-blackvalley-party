---
Title: Competitions rules, schedule & tickets
Date: 2022-06-13
icon: img/ticket.svg
---

We've added [competition rules][compo rules], our party [schedule] and
announced when tickets will be made available for purchase!

<!--more-->

~~[Tickets] will be made available on Friday the 17th of June, at 18:00 CEST.
We've also added some more information about the event on the [about
section] on the front-page.~~

Update: All tickets have sold out!

[compo rules]: {{< ref "/compos.md#competition-rules" >}}
[schedule]: {{< ref "/schedule.md" >}}
[Tickets]: https://fienta.com/black-valley-2022
[about section]: {{< ref "/_index.md#about" >}}
