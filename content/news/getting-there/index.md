---
Title: Getting to the party!
Date: 2022-07-14
icon: img/walk.svg
---

The party starts tomorrow, so here's some last-minute information ion how
to get there!

<!--more-->

### The basics

First of all, let's start with the most important bit. Our address is:

{{< address >}}
- Kruttverket
- Arnljot Gellines Vei 41 B
- 0657 Oslo
{{< /address >}}

The entrance is from the west-side of the building. There will be signs
telling you where to go, and a banner showing where to enter.

You can find it on Google Maps here: https://goo.gl/maps/3NHhVf64ozt9BYRU8

However, due to limited parking options, we recommend people to use public
transportation. The venue has zero parking spots, the only option is
street parking, which is both hard to find and somewhat costy. Taking the
subway or bus and walking from there is a lot easier.

### Walking from Helsfyr T

To make things a bit easier, Offwhite and Susencrusen has made a little
video showing how to walk from the nearest easily reachable public
tranportation hub, [Helsfyr Subway / Bus stop][helsfyr], which you can
watch below! The video also makes a stop at Etterstad Sør along the way,
which is the nearest bus stop, reachable by bus #37.

{{< youtube-embed id="_EpaDLu2UNM" >}}

Hopefully, this should be enough to get you to the venue! See you there! 🤩

[helsfyr]: https://goo.gl/maps/bTxgXbq9DP9ScqMX7
[on YouTube]: https://www.youtube.com/shorts/Ptx2DyPQ2VY
