---
Title: Post-party stats
Date: 2022-10-10
icon: img/bar-chart.svg
---

It's been a while since Black Valley 2022, and we've been planning on
publishing some information about how the party went. This has taken
longer than we wanted, apologies for that.

<!--more-->

## Tickets

Here's a breakdown of the tickets we sold, by type:

| Ticket type      | Amount |
|:-----------------|-------:|
| Visitor ticket   |     82 |
| Supporter ticket |      2 |
| Free ticket      |      6 |

That's a total of **93 tickets**. Out of these **89 tickets** were scanned
at the door in exchange for a wristband and votekey.

The free tickets are tickets given to people who contributed to the party
without being crew members. This was given out to people who worked on the
invitation intro, played DJ sets at the party or provided the SceneSat
stream.

## Competitions

We held 7 competitions, with a total of **51 contributions** across all
disciplines!

To pick the winners, 75 votes were cast by 75 registered voters. This means
that **almost 85%** of the visitors voted!

## Budget

We want to be a transparent organization, so we think it makes sense to
publish how the finances went. This way, people can get to know what their
entrance fee was spent on.

| Item                     |        Amount |
|:-------------------------|--------------:|
| Rent, Venue              | -12,000.00 kr |
| Rent, Bar                |  -4,000.00 kr |
| Rent, PA                 | -17,561.25 kr |
| Rent, Projector screen   |  -4,212.50 kr |
| Rent, Tables             |  -1,258.00 kr |
| Print, Crew t-shirts     |  -1,548.00 kr |
| Print, Wristbands        |    -694.00 kr |
| Print, Nametags + Banner |    -506.00 kr |
| Hand sanitizers          |    -239.60 kr |
| Floor mats               |  -1,990.00 kr |
| Ear plugs                |    -447.00 kr |
| Strips                   |    -278.00 kr |
| Silver duct tape         |    -444.40 kr |
| Black duct tape          |    -258.00 kr |
| Misc equipment (Biltema) |    -233.40 kr |
| Misc equipment (Jula)    |    -398.40 kr |
| Floor Protection Paper   |    -438.00 kr |
| Rigging gloves           |    -319.20 kr |
| Pizza rigging            |  -1,380.00 kr |
| Banking fee              |      -5.00 kr |
| Ticket sale              |  45,789.25 kr |
| Sponsorship Collabora    |   7,164.73 kr |

This leaves us with the result of **4,743.23 kr** left over! The surplus will
be spent on future events.

That's pretty much it for this post! As we said during the closing event, we
intend on making a Black Valley 2023 as well, please stay tuned for further
announcements on that end!
