---
Title: Exclusive t-shirt competition
Date: 2022-06-05
icon: img/shirt.svg
images:
  - /news/tshirt-compo/twitter-card.jpg
---

We've just added a special, new [t-shirt remix competition][tshirt-compo] to our list of compeitions!

<!--more-->

![Anonymous scener with a Black Valley t-shirt](tshirt-compo.jpg)

[tshirt-compo]:{{< ref "/compos.md#exclusive-t-shirt-remix-compo" >}}
