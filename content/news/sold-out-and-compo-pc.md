---
Title: Sold out again, and beefier compo pc!
Date: 2022-06-25
icon: img/ticket.svg
---

The second round of tickets sold out in just over 15 minutes, even though
we forgot to add the link to the ticket sale back to the website!
:sweat_smile:

<!--more-->

We're terribly sorry for not adding the link in time, but the tickets are
gone, and we can't responsibly squeeze in more people. We're really looking
forward to seeing you in three weeks! :tada:

In other news, we have also upgraded our compo PC to a beefier one! We're
now sporting an RTX 3080 Ti GPU, as well as a faster CPU and more memory.
That's going to make even the heaviest of demos run as smooth as possible!