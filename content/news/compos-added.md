---
Title: Competitions page added
Date: 2022-05-31
---

We've just added [a page]({{< ref "/compos.md" >}}) listing the
competitions we are hosting at Black Valley 2022!

<!--more-->

Since this is the first year we're arranging Black Valley, we're planning
on starting out small, so there's two graphics, two music and two real-time
competitions. More information and compo rules will follow shortly!

...And who knows, maybe we'll throw in another surprise as well? :wink:
