---
Title: Visitors
menu:
  footer:
    parent: pages
    weight: 5
---

These awesome people registered as attendees on [demoparty.net](https://demoparty.net):

{{< visitors "black-valley/black-valley-2022" >}}

If you want to appear in the list above, go to [this page](https://www.demoparty.net/black-valley/black-valley-2022) and register.
