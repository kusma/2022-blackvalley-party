---
title: Competitions
menu:
  main:
    weight: 3
  footer:
    parent: pages
    weight: 3
---

The following competitions will be held at Black Valley:

### Graphics

- {{< compo-heading >}}[Pixeled graphics]({{< ref "#pixeled-graphics" >}}){{< /compo-heading >}}
  Draw something that looks awesome with limited resolution and color
  palette!

- {{< compo-heading >}}[Freestyle graphics]({{< ref "#freestyle-graphics" >}}){{< /compo-heading >}}
  Use all the techniques, pixels and colors you want. Go nuts!

### Music

- {{< compo-heading >}}[Oldskool music]({{< ref "#oldskool-music" >}}){{< /compo-heading >}}
  Make your old computer go blip-blop, and make the audience dance!

- {{< compo-heading >}}[Newschool music]({{< ref "#newschool-music" >}}){{< /compo-heading >}}
  Go gelastic in your favorite DAW and create something that blows the
  roof off the venue!

### Real-time

- {{< compo-heading >}}[Combined intro]({{< ref "#combined-intro" >}}){{< /compo-heading >}}
  Show us what you can cram into the smallest amount of bytes!

- {{< compo-heading >}}[Combined demo]({{< ref "#combined-demo" >}}){{< /compo-heading >}}
  Do the best you can, regardless of size. Any platform goes!

### Exclusive: T-shirt remix!

This is our exclusive competition, where you make *your own* Black Valley
party t-shirt! We provide you with the [SVG of our logo] as a "reference
design", and you can choose how much or how little of it you use! The
result should be a physical t-shirt that you bring to the party. Have fun!

## Competition rules

### General Rules

These rules applies to all competitions.

1. ###### Remote entries
   We allow remote entries in all competitions, except for the t-shirt
   remix competition. Remote entries needs to be delivered to us [via
   email][contact] at latest the day before the party starts. In addition,
   you need make yourself available via email or on Discord for any
   troubleshooting / questions during the party.

2. ###### Respect the deadlines
   We publish deadlines ahead of time, and these must be respected. If
   you fail to deliver a working entry in time for the deadline, we might
   not include your entry. If you're in the need of more time to finish
   your entry, contact us; we're some times able to be flexible. But no
   guarantees.

3. ###### No prereleased material
   We do not allow previously released material to be entered into any of
   our competitions. This includes "light" remixes of content. All
   productions needs to stand on their own creative legs.

4. ###### One competition per entry
   We do not allow an entry to be entered into multiple competitions. Pick
   the most appropriate competition and enter it in that. This only
   applies to the whole production; you *can* for instance enter the
   soundtrack for a demo in one of the music competitions.

5. ###### Only one entry per participant per competition
   If you’re entering a compofiller, please have the decency to make up a
   fake name :wink:

6. ###### Other reasons for disqualification
   We reserve the right to disqualify entries that are too unserious,
   boring, bad, disrespectful or otherwise illegal or inappropriate
   content. In fact, we reserve the right to disqualify entries for any
   reason we find reasonable.

7. ###### We won't release disqualified entries
   In case an entry doesn't make the cut for some reason, we of course
   won't release it after the party either. This way you can hand it in
   at some other competition in the future.

8. ###### License
   When you enter a production in a competition, you implicitly grant us a
   world-wide, no-revokable license to display and redistribute your
   entry. The reason for this is that we need the legal rights to display
   the entry in the competition, and to upload the contributions to
   [Scene.org](https://files.scene.org/) for archival reasons after the
   party is over.

### Graphics competitions

We have some rules that apply to all graphics competitions, as well as some
specific rules per competition.

1. ###### All techniques are allowed
   We allow all creative techniques, draw, scan, convert, whatever. Just
   make sure your entry is all your own original content.

2. ###### Deliver work-in-progress snapshots
   Entries must be supplied with three representative work-in-progress
   snapshots. These *will* be shown during the competition.

3. ###### Please deliver a PNG version
   We can't support showing every conceivable format correctly, so please
   deliver an unmodified PNG version together of your entry. You're free
   to deliver other formats in addition, but the PNG file is what we'll
   show in the competition.

#### Pixeled graphics

1. ###### Maximum 256 colors, 8 bit per component
   The pixeled graphics competition is intended for up to 256 color,
   paletted images. You're of course free to use fewer color or more
   restrictive palettes than this.

2. ###### Max resolution 320x256 pixels
   The maximum width and height of the entries are 320 and 256,
   respectively. Do note that our big-screen is 16:9, so if you want your
   entry to fill as much as possible of the screen, consider using
   a 320x180 resolution.

#### Freestyle graphics

There's no color or resolution limit in this competition, but bear in
mind that the image will be shown in 1920x1080 resolution on the
projector, so consider delivering an additional PNG at that resolution if
your entry is larger. Otherwise, we can't guarantee how the image will be
rescaled to fit the screen.

### Music competitions

We have some rules that apply to all music competitions, as well as some
specific rules per competition.

1. ###### Please deliver a pre-rendered version
   We can't support playing every conceivable format correctly, so please
   deliver a pre-rendered, unmodified MP3 version together with your
   entry. You're free to deliver other formats in addition, but the MP3
   file is what we'll play in the competition.

2. ###### Maximum 4 minutes play-time
   Music competitions tend to get tedious for the audience if there's a
   lot of long entries. To avoid that, and to make sure we stay on
   schedule, we've decided to set a 4 minute play-limit on entries. Make
   sure your entry stays within that limit, otherwise we might cut it
   before it's done!

#### Oldskool music

The definition of "oldskool" isn't something everyone agrees 100% on, and
we don't plan on making a very strict definition. Instead we give a few
guidelines about what we think this is, and take the debate once we have
entries that we feel fall outside our definition.

1. ###### Old computers or games consoles
   The entry should be made for 80s or early 90s-era computers or game
   consoles. This means, no fantasy-console music in this competition.

2. ###### No streamed music
   While some of these systems are techically capable of playing back MP3
   or WAV files, those entries belong in the newschool music competition.

3. ###### Tracked music limitations
   Tracked music formats should be limited to 4 channel, 8bit samples. In
   other words, Amiga / Protracker MODs.

We will list the platform on the beam-slides for all entries.

#### Newschool music

There's no limit on what kind of format or tools used in this competition.

Just follow the other rules, and you should be good!

That being said, Xerxes will automatically be disqualified.

### Real-time competitions

We don't differentiate between platforms in the real-time competitions.
Instead, we expect our audience to take the platforms into account when
voting, at their own discretion.

There's several computers available that we can use to show entries on,
here's an overview:

1. ###### Windows / Linux PC:
   - AMD Ryzen 9 5900X CPU
   - 64 GB RAM
   - Gigabyte RTX 3080 Ti OC 12G GPU
   - Windows 11 21H2
   - Ubuntu 22.04 LTS
2. ###### Commodore 64:
   - 1541 Ultimate II
   - SidFX with 6581R3 and 8580R5
   - USB and/or MicroSD card reader
3. ###### Commodore Amiga 500:
   - Kickstart 1.3
   - 512 kB chip RAM + 512 kB fakefast
   - Floppy emulator
4. ###### Commodore Amiga 1200:
   - Kickstart 3.2
   - TF1260, 68060@50MHz
   - 128 MB Fast RAM
   - 2 MB Chip RAM
   - Compact flash and/or SD card reader
5. ###### Atari Falcon:
   - Motorola 68030 CPU
   - 14 MB RAM
   - Compact flash reader

We also have much more equipment available, please [contact] us if you're
planning on bringing something for some other platform. If we don't have
the right machine, you can always bring your own to the party, assuming we
have the ability to get a reasonable video / audio signal out of it.

We will list the platform on the beam-slides for all entries.

###### Maximum 8 minutes run-time

While we know that certain oldskoolers love their scrollmos to run on
forever, it's often not too fun to watch in a competition setting. To
avoid that, and to make sure we stay on schedule, we've decided to set an
8 minute run-time limit on entries. Make sure your entry stays within that
limit, otherwise we might cut it before it's done!

#### Combined intro

1. ###### Maximum executable size
   The entry must be maximum 65536 bytes (64 kB) large. This includes any
   operating-system defined padding.

2. ###### Single executable file
   The entry must be a single executable file, without depending on any
   non-system default code or data-files.

We will list the size of the entry on the beam-slides for all entries.

#### Combined demo

There's no limit on size in this competition, but do keep in mind that the
entry needs to be practically possible to redistribute after the party. So
please try to keep the file-size within reason.

Also, please to make the distribution as non-intrusive as possible. In
other words, don't require extra installation steps, don't needlessly
write files to the file-system... You know. Try to be nice to the system
that the production is run on.

### T-shirt remix

To display this in the competition, you (or someone else on your behalf)
will have to wear the T-shirt in front of the big-screen. In addition, we
will take photos of the t-shirt for voting purposes.

For the obvious practical reasons, the T-shirt competition can only be
entered in-person.

----

And that's pretty much it! If there's anything that unclear, or you
otherwise are unsure about, don't hesitate to [contact] us and ask!

[SVG of our logo]: ../../img/logo.svg
[contact]: {{< ref contact.md >}}
